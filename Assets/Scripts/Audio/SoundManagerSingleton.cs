﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManagerSingleton : MonoBehaviour
{
    public AudioSource sfxSource;
    public AudioSource musicSource;
    public AudioMixer mixer;
    public Slider[] slider;
    public UIController uiController;
    public static bool IsMenu = true;
    protected SoundManagerSingleton() { }

    public static SoundManagerSingleton Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        //slider[0] = GameObject.Find("o_Master_Slider").GetComponent<Slider>();
        //slider[1] = GameObject.Find("o_BGM_Slider").GetComponent<Slider>();
        //slider[2] = GameObject.Find("o_SFX_Slider").GetComponent<Slider>();
        slider[0].value = PlayerPrefs.GetFloat("MasterVolume", 0.75f);
        slider[1].value = PlayerPrefs.GetFloat("BGMVolume", 0.75f);
        slider[2].value = PlayerPrefs.GetFloat("SFXVolume", 0.75f);
    }

    private void Update()
    {
        if (slider[0] == null && IsMenu)
        {
            uiController = GameObject.Find("UIController").GetComponent<UIController>();
            Debug.Log("Missing Slider");
            slider[0] = uiController.VolSliders[0];
            slider[1] = uiController.VolSliders[1];
            slider[2] = uiController.VolSliders[2];
            slider[0].value = PlayerPrefs.GetFloat("MasterVolume");
            slider[1].value = PlayerPrefs.GetFloat("BGMVolume");
            slider[2].value = PlayerPrefs.GetFloat("SFXVolume");
        }
    }

    public void PlaySFX(AudioClip clip)
    {
        sfxSource.clip = clip;
        sfxSource.Play();
    }

    public void PlayMusic(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.Play();
    }
    
    public void ChangeMasterVol(float value)
    {
        float val = slider[0].value;
        mixer.SetFloat("MasterVol", Mathf.Log10(val) * 20);
        PlayerPrefs.SetFloat("MasterVolume", val);
    }
    
    public void ChangeMusicVol(float value)
    {
        float val = slider[1].value;
        mixer.SetFloat("BGMVol", Mathf.Log10(val) * 20);
        PlayerPrefs.SetFloat("BGMVolume", val);
    }
    
    public void ChangeSFXVol(float value)
    {
        float val = slider[2].value;
        mixer.SetFloat("SFXVol", Mathf.Log10(val) * 20);
        PlayerPrefs.SetFloat("SFXVolume", val);
    }

    public void MuteOrUnmute()
    {
        musicSource.mute = !musicSource.mute;
    }
    
    public void Unmute()
    {
        musicSource.mute = false;
    }
}
