﻿using UnityEngine;

public class SkinOptions : MonoBehaviour
{
    #region SpriteOptions
    [Header("Sprite Options")] 
    public Sprite[] bodyOption;
    public Sprite[] rightUpperArmOptions;
    public Sprite[] rightForeArmOptions;
    public Sprite[] rightHandOptions;
    public Sprite[] leftUpperArmOptions;
    public Sprite[] leftForeArmOptions;
    public Sprite[] leftHandOptions;
    [Space]
    public Sprite[] rightThighOptions;
    public Sprite[] rightKneeOptions;
    public Sprite[] leftThighOptions;
    public Sprite[] leftKneeOptions;
    [Space]
    public Sprite[] neckWearOptions;
    [Space]
    public Sprite[] headOptions;
    public Sprite[] earsOptions;
    [Space]
    public Sprite[] eyesBlowOptions;
    [Space]
    public Sprite[] backHatOptions;
    public Sprite[] frontHatOptions;
    [Space]
    public Sprite[] faceOptions;
    [Space]
    public Sprite[] mouthNoseOptions;
    [Space]
    public Sprite[] leftEyeOptions;
    public Sprite[] rightEyeOptions;
    [Space]
    public Sprite[] leftHairOptions;
    public Sprite[] bangHairOptions;
    public Sprite[] rightHairOptions;
    public Sprite[] ahogeHairOptions;
    public Sprite[] backHairOptions;
    [Space]
    public Sprite[] tailOptions;
    

    #endregion

}
