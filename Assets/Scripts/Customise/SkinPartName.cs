﻿public class SkinPartName
{
    #region String
    public const string _00BODY = "Body"; //B
    public const string _01RIGHT_UPPERARM = "RightUpperArm"; //UA_R
    public const string _02RIGHT_FOREARM = "RightForeArm"; //FA_R
    public const string _03RIGHT_HAND = "RightHand"; //HN_R
    public const string _04LEFT_UPPERARM = "LeftUpperArm"; //UA_L
    public const string _05LEFT_FOREARM = "LeftForeArm"; //FA_L
    public const string _06LEFT_HAND = "LeftHand"; //HN_L
    
    public const string _07RIGHTTHIGH = "RightThigh"; //T_R
    public const string _08RIGHTKNEE = "RightKnee"; //K_R
    public const string _09LEFTTHIGH = "LeftThigh"; //T_L
    public const string _10LEFTKNEE = "LeftKnee"; //K_L
    
    public const string _11NECKWEAR = "NeckWear"; //NK
    
    public const string _12HEAD = "Head"; //HD
    public const string _13EAR = "Ear"; //ER
    
    public const string _14EYESBLOW = "Eyebrow"; //EB
    
    public const string _15BACKHAT = "BackHat"; //BH
    public const string _16FRONTHAT = "FrontHat"; //FH
    
    public const string _17FACE = "Face"; //FC
    
    public const string _18MOUTHNOSE = "MouthNose"; //MN 
    
    public const string _19LEFTEYE = "LeftEye"; //E_L
    public const string _20RIGHTEYE = "RightEye"; //E_R
    
    public const string _21LEFTHAIR = "LeftHair"; //H_L
    public const string _22BANGHAIR = "BangHair"; //H_BN
    public const string _23RIGHTHAIR = "RightHair"; //H_R
    public const string _24AHOGEHAIR = "AhogeHair"; //H_A
    public const string _25BACKHAIR = "BackHair";//H_B
    
    public const string _26TAIL = "Tail"; //TL
    
    #endregion

    #region Index

    public const int IDX_BODY = 0;
    public const int IDX_RIGHT_UPPERARM = 1;
    public const int IDX_RIGHT_FOREARM = 2;
    public const int IDX_RIGHT_HAND = 3;
    public const int IDX_LEFT_UPPERARM = 4;
    public const int IDX_LEFT_FOREARM = 5;
    public const int IDX_LEFT_HAND = 6;
    public const int IDX_RIGHTTHIGH = 7;
    public const int IDX_RIGHTKNEE = 8;
    public const int IDX_LEFTTHIGH = 9;
    public const int IDX_LEFTKNEE = 10;
    public const int IDX_NECKWEAR = 11;
    public const int IDX_HEAD = 12;
    public const int IDX_EAR = 13;
    public const int IDX_EYESBLOW = 14;
    public const int IDX_BACKHAT = 15;
    public const int IDX_FRONTHAT = 16;
    public const int IDX_FACE = 17;
    public const int IDX_MOUTHNOSE = 18;
    public const int IDX_LEFTEYE = 19;
    public const int IDX_RIGHTEYE = 20;
    public const int IDX_LEFTHAIR = 21;
    public const int IDX_BANGHAIR = 22;
    public const int IDX_RIGHTHAIR = 23;
    public const int IDX_AHOGEHAIR = 24;
    public const int IDX_BACKHAIR = 25;
    public const int IDX_TAIL = 26;

    public const int IDX_EYES = 101;
    public const int IDX_Hair = 102;

    #endregion
}
