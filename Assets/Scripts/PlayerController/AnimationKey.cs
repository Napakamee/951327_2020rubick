﻿
public class AnimationKey
{
    public const string MeleeAttack = "MeleeAttack";
    public const string Speed = "Speed";
    public const string Angle = "Angle";
    public const string IsFiring = "IsFiring";
    public const string IsFiringPistol = "IsFiringPistol";
    public const string IsAttack = "IsAttack";
    public const string IsMelee = "IsMelee";
    public const string IsEquipGun= "IsEquipGun";
    public const string Is3D = "Is3D";
    public const string IsPistol = "IsPistol";
}
