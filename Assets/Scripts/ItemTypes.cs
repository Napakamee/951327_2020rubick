﻿public enum ItemType
    {
        Bat = 0,
        Hammer = 1,
        Pistol = 2,
        Sniper = 3,
        MachineGun = 4,
        ShotGun = 5
    }

